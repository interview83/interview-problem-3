## Create the following program

For a game called "The Lost Wizard of Artemia" you need to create the "teleportation spell" routine.

Inputs:
* Cities.csv = a list of cities from Artemia
* Characters.csv = the list of characters from the game

The teleportation spell:
1. Read the two files and randomly place the characters in one city making sure that there is no more than one character in one city. Write the output to a csv file called "InitialHomesOfArtemia.csv".
2. Create a the "teleportation spell" routine: 4 random characters change location to a random different city. If the destination city already has two or more other characters, the teleported characted is send to "TheNowhere" and will not be teleported again during other spells. Print the result to the console.
3. Simulate the game: Run the spell procedure 42 times, with a 40ms time between the start of each spell. Track the character evolution.
4. After the 42 "spells" are performed, write a csv file containing the path of all the characters. The header of the csv should be the name of the characters.

#### Aditional instructions

* Use programming language, libraries and db of your choice.
* Any kind of online and offline documentation and resources can be used.
* Fork this gitlab repository into a new one owned by you and private and commit your solution in a development branch.
* You should spend 1-2h trying to create this program.
* Even if you don't have a final solution it is important to commit your work.
* Please leave comments in the code to help us understand your thought proces.

#### Handle the repository

- Fork the repository. 
- In your forked repository:
  - Go to Settings > General > Visibility and set the project visibility to Private.
  - Go to Settings > Members > Invite Member and add @spaceapps-rsi as Reporters so we can follow your progress.
- Complete the test
- Let us know with an email when you're done 
